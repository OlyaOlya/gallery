from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^category_page/(?P<dir_id>[0-9]+)/$', views.category_page),
    url(r'^category_delete/$', views.category_delete, name='category_delete'),
    url(r'^image_delete/(?P<img_id>[0-9]+)/$', views.image_delete, name='image_delete'),
    url(r'^add_category/$', views.add_category, name='add_category'),
    url(r'^listing/$', views.listing, name='listing'),
    url(r'^upload_file/$', views.upload_file, name='upload_file'),
    url(r'^sort_name_a/$', views.sort_name_a, name='sort_name_a'),
    url(r'^sort_name_z/$', views.sort_name_z, name='sort_name_z'),
    url(r'^sort_date_a/$', views.sort_date_a, name='sort_date_a'),
    url(r'^sort_date_z/$', views.sort_date_z, name='sort_date_z'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^restricted/', views.restricted, name='restricted'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^edit_category/', views.edit_category, name='edit_category'),


]