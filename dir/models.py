from django.db import models
from django.contrib.auth.models import User
from mptt.models import MPTTModel, TreeForeignKey
from django.contrib.auth.models import User


class AllDir(MPTTModel):
    name = models.CharField(max_length=50, unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)

    def __str__(self):
        return self.name

    class MPTTMeta:
        order_insertion_by = ['name']


class NewImage(models.Model):
    dir = models.ForeignKey(AllDir, blank=True)
    nameIm = models.ImageField(upload_to='all_img', blank=True)
    date = models.DateTimeField(auto_now_add=True)



