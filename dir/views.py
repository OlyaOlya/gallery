from django.shortcuts import redirect
from .models import AllDir, NewImage
from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.contrib.auth import login
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from dir.forms import CategoryForm, ImageForm, UserForm


#пагинация
def listing(request, image_list):
    paginator = Paginator(image_list, 6)  # 6 картинок на странице
    page = request.GET.get('page')
    try:
        images = paginator.page(page)
    except PageNotAnInteger:
        images = paginator.page(1)
    except EmptyPage:
        images = paginator.page(paginator.num_pages)
    return images


def index(request):
    image_list = NewImage.objects.all()
    images = listing(request, image_list)
    return render(request, "dir/index.html", {'nodes': AllDir.objects.all(),
                                              'images': images})


# сортировка по имени в алф. порядке
def sort_name_a(request):
    image_list = NewImage.objects.filter().order_by("nameIm")
    images = listing(request, image_list)
    return render(request, "dir/index.html", {'nodes': AllDir.objects.all(),
                                              'images': images})


# сортровка по имени в обратном алф. порядке
def sort_name_z(request):
    image_list = NewImage.objects.filter().order_by("nameIm").reverse()
    images = listing(request, image_list)
    return render(request, "dir/index.html", {'nodes': AllDir.objects.all(),
                                              'images': images})


# сортировка по дате добавления
def sort_date_a(request):
    image_list = NewImage.objects.order_by('date')
    images = listing(request, image_list)
    return render(request, "dir/index.html", {'nodes': AllDir.objects.all(),
                                              'images': images})


# сортировка по дате добавления в обратном порядке
def sort_date_z(request):
    image_list = NewImage.objects.order_by('date').reverse()
    images= listing(request, image_list)
    return render(request, "dir/index.html", {'nodes': AllDir.objects.all(),
                                              'images': images})


# загрузка файла
@login_required
def upload_file(request):
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/dir')
    else:
        form = ImageForm()
    return render(request, 'dir/upload.html', {'form': form})


# вывод по категориям
def category_page(request, dir_id):
    image_list = NewImage.objects.filter(dir_id=dir_id)
    images= listing(request, image_list)
    current_category = AllDir.objects.get(id=dir_id)
    root_category_id = current_category.get_root().id
    return render(request, "dir/index.html", {'nodes': AllDir.objects.all(),
                                              'current_category': current_category,
                                              'root_category_id': root_category_id,
                                              'images': images})


# удалить картинку
@login_required
def image_delete(request, img_id):
    obj = get_object_or_404(NewImage, pk=img_id)
    success_url = redirect('index')
    NewImage.objects.filter(pk=obj.pk).delete()
    return success_url


# удалить категорию
@login_required
def category_delete(request):
    text = request.GET.get('str1', None)
    text = str(text)
    text1 = text.split(";")
    for item in text1:
        AllDir.objects.get(id=item).delete()
        '''if len(NewImage.objects.get(dir_id=item)) > 0:
            for img in range(len(NewImage.objects.get(dir_id=item))):
                NewImage.objects.get(dir_id=img).nameIm.delete()
                NewImage.objects.get(dir_id=img).delete()'''
    return HttpResponseRedirect('/dir')


# добавить новую категорию.
@login_required
def add_category(request):
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return index(request)
        else:
            print (form.errors)
    else:
        form = CategoryForm()
    return render(request, 'dir/add_category.html', {'form': form}, {'nodes': AllDir.objects.all()})


# попытка сделать редактирование
def edit_category(request):
    dir_id = request.GET.get('str1', None)
    post =AllDir.objects.get(id=dir_id)
    if request.method == "POST":
        form = CategoryForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.name = request.name
            post.parent = request.parent
            post.save()
            return redirect('category_page', id=post.dir_id)
    else:
        form = CategoryForm(instance=post)
    return render(request, 'dir/edit_category.html', {'form': form})


# регистрация
def register(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            registered = True
        else:
            print(user_form.errors)
    else:
        user_form = UserForm()
    return render(request, 'dir/register.html', {'user_form': user_form,
                                                 'registered': registered} )


# авторизация
def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/dir')
            else:
                return HttpResponse("Your account is disabled.")
        else:
            print("Invalid login details: {0}, {1}".format(username, password))
            return HttpResponse("Invalid login details supplied.")
    else:
        return render(request, 'dir/login.html', {})


@login_required
def restricted(request):
    return HttpResponse("Since you're logged in, you can see this text!")


# выход из системы
@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/dir/')