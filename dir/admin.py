from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from .models import AllDir,  NewImage


class AlldirAdmin(MPTTModelAdmin):
    models = AllDir
    list_display = ('id', 'name', 'parent')
    list_filter = ('parent',)


class NewimageAdmin(admin.ModelAdmin):
    models = NewImage
    list_display = ('id', 'dir', 'nameIm', 'date')


admin.site.register(AllDir, AlldirAdmin)
admin.site.register(NewImage, NewimageAdmin)

