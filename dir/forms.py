from django import forms
from dir.models import AllDir, NewImage
from django.contrib.auth.models import User


class CategoryForm(forms.ModelForm):
    name = forms.CharField(max_length=128, help_text="Please enter the category name.")

    class Meta:
        model = AllDir
        fields = ('name', 'parent')


class ImageForm(forms.ModelForm):
  class Meta:
    model = NewImage
    fields = ('dir', 'nameIm')


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')
